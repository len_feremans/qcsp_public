package core;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import util.IOUtils;

public class TestQCSP {
	
	File sequenceTxt = new File("./temp/sequence_unittest.txt");
	
	@Test
	public void runOnExampleCasePaper() throws IOException {
		//saving a,b,c,_,_,_,_,b,a,c
		makeDataset("a b c None None None None b a c");
		QCSP algo = new QCSP(sequenceTxt, 2.0, 3, 2);
		algo.run(true);
	}
	
	@Test
	public void runOnExampleCase2() throws IOException {
		makeDataset("a_b_cd_____ab___");
		QCSP algo = new QCSP(sequenceTxt, 2.0, 3, 2);
		algo.run(true);
	}
	
	@Test
	public void runOnExampleCase3() throws IOException {
		makeDataset("abcd_____ab");
		QCSP algo = new QCSP(sequenceTxt, 1.0, 3, 2);
		algo.run(true);
	}
	
	@Test
	public void runOnExampleCase4() throws IOException {
		makeDataset("aabbcd___________ab__c");
		QCSP algo = new QCSP(sequenceTxt, 2.0, 3, 2);
		algo.run(true);
	}
	
	@Test
	public void runOnExampleCase5() throws IOException {
		makeDataset("a a b b b b None a b");
		QCSP algo = new QCSP(sequenceTxt, 2.0, 3, 2);
		algo.run(true);
	}
	
	
	@Test
	public void runOnMobyFragment() throws IOException {
		File data = new File("./data/moby_dick_gutenberg_sentences_clean.txt");
		QCSP algo = new QCSP(data, 2.0, 5, 50);
		algo.run(true);
	}
	
	private void makeDataset(String str) throws IOException {
		System.out.println("TESTING: " + str);
		IOUtils.saveFile(str, sequenceTxt);
	}
}
