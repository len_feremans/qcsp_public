package core;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import util.DatLabFileToNormalFile;
import util.IOUtils;

public class TestQCSP_Sparse {
	
	File sequenceTxt = new File("./temp/sequence_sparse_unittest.txt");
	File labelsTxt = new File("./temp/sequence_labels_unittest.txt");
	
	@Test
	public void runOnExampleCasePaperSparse() throws IOException {
		//saving a,b,c,_,_,_,_,b,a,c
		makeDataset("abc____bac");
		QCSP_Sparse algo = new QCSP_Sparse(sequenceTxt,labelsTxt, 120, 3, 2);
		algo.run(true);
	}
	
	@Test
	public void runOnExampleCaseItemsetSparse() throws IOException {
		//saving a,b,c,_,_,_,_,b,a,c
		makeDataset("a(be)(dc)____abc");
		QCSP_Sparse algo = new QCSP_Sparse(sequenceTxt, labelsTxt, 60, 3, 10);
		algo.run(true);
	}
	
	@Test
	public void runOnExampleCase2Sparse() throws IOException {
		makeDataset("a_b_cd_____ab___");
		QCSP_Sparse algo = new QCSP_Sparse(sequenceTxt, labelsTxt, 60, 3, 2);
		algo.run(true);
	}
	
	@Test
	public void runOnExampleCase3Sparse() throws IOException {
		makeDataset("abcd_____ab");
		QCSP_Sparse algo = new QCSP_Sparse(sequenceTxt, labelsTxt, 60, 3, 2);
		algo.run(true);
	}
	
	@Test
	public void runOnExampleCase4Sparse() throws IOException {
		makeDataset("aabbcd___________ab__c");
		QCSP_Sparse algo = new QCSP_Sparse(sequenceTxt, labelsTxt, 60, 3, 2);
		algo.run(true);
	}
	
	@Test
	public void runOnExampleCase5Sparse() throws IOException {
		makeDataset("bbbbac");
		QCSP_Sparse algo = new QCSP_Sparse(sequenceTxt, labelsTxt, 60, 3, 2);
		algo.run(true);
	}
	@Test
	public void runOnMobyFragmentSparse() throws IOException {
		File singleFile = new File("./data/moby_dick_gutenberg_sentences_clean.txt");
		File seq = new File("./temp/", singleFile.getName() + "_gap.seq");
		File label = new File("./temp/", singleFile.getName() + "_gap.lab");
		DatLabFileToNormalFile.convertToDataLab(singleFile, seq, label);
		//sparse format needs timestamps
		String[] sequence = IOUtils.readFileUntil(seq, -1).get(0).split(" ");
		StringBuffer new_seq_content = new StringBuffer();
		Calendar now = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for(String item: sequence) {
			String timestamp = formatter.format(now.getTime());
			new_seq_content.append(String.format("%s,%s\n", timestamp,item));
			now.add(Calendar.MINUTE, 1);
		}
		IOUtils.saveFile(new_seq_content.toString(), seq);
		QCSP_Sparse algo = new QCSP_Sparse(seq,label, 60, 4, 50);
		algo.run(true);
	}
	
	private void makeDataset(String str) throws IOException {
		System.out.println("TESTING: " + str);
		//make labels file
		String str2 = str.replace("(", "").replace(")", "");
		List<String> uniqueLabels = new ArrayList<String>();
		for(int i=0; i<str2.length(); i++) {
			String current = str2.substring(i,i+1);
			if(!uniqueLabels.contains(current)) {
				uniqueLabels.add(current);
			}
		}
		String labelsFileContent = "";
		for(String label: uniqueLabels) {
			labelsFileContent += String.format("%s\n", label);
		}
		IOUtils.saveFile(labelsFileContent, labelsTxt);
		//make sequence File in sparse formate, e.g. datetime,item1,item2...
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sequenceFileContent = "";
		Calendar now = Calendar.getInstance();
		for(int i=0; i<str.length(); i++) {
			String current = str.substring(i,i+1);
			int startIndex = i;
			if(current.equals("(")) {
				for(;i<str.length(); i++) {
					if(str.substring(i,i+1).equals(")")) {
						break;
					}
				}
				current = str.substring(startIndex+1,i);
			}
			if(!current.equals("_")) {
				sequenceFileContent += formatter.format(now.getTime()) + ",";
				for(int j=0; j < current.length(); j++) {
					sequenceFileContent += uniqueLabels.indexOf(current.substring(j, j+1)) + ",";
				}
				sequenceFileContent = sequenceFileContent.substring(0, sequenceFileContent.length() -1) + "\n";
			}
		    now.add(Calendar.MINUTE, 1);
		}
		IOUtils.saveFile(sequenceFileContent, sequenceTxt);
		IOUtils.printHead(sequenceTxt, 50);
		IOUtils.printHead(labelsTxt, 50);
	}
}
