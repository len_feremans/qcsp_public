import random
import collections
#numpy.random.randint 

'''
The \emph{Synthetic} dataset is created by generating a single sequence of $2000$ items randomly selected between $6$ and $50$. 
Next we insert the sequential pattern $(1,2,3,4,5)$ with at most $5$ gaps at $40$ random non-overlapping locations. '''


def generate_dataset(no_random_symbols, pattern_length, sequence_length, no_instances_pattern_org):
    sequence = [1 for i in range(0,sequence_length)]
    random_symbols_start = pattern_length + 1
    for  i in range(0,sequence_length):
        sequence[i] = random.randint(random_symbols_start,no_random_symbols) #inclusive
    #sequence should occur
    #no_instances_pattern_org = int(pattern_length * sequence_length/float(no_random_symbols))
    no_instances_pattern = no_instances_pattern_org
    while no_instances_pattern > 0:
        seq_position =  random.randint(0, sequence_length - pattern_length * 2)
        pattern_occurrence =  [i for i in range(1,pattern_length+1)] #e.g. (1,2,3,4,5)
        #check non-overlapping
        overlaps = False
        for item in pattern_occurrence:
            if item in sequence[seq_position: seq_position+pattern_length * 2]:
                overlaps = True
        if overlaps:
            continue
        #make gaps    
        no_gaps = pattern_length
        while no_gaps > 0:
            gap_position =  random.randint(0, len(pattern_occurrence))
            pattern_occurrence = pattern_occurrence[0:gap_position] + [None] + pattern_occurrence[gap_position:]
            no_gaps-=1
        #put pattern with gaps in sequence
        for j in range(0,len(pattern_occurrence)):
            if pattern_occurrence[j] != None:
                sequence[seq_position+j] = pattern_occurrence[j]
        no_instances_pattern -= 1
    distribution = collections.Counter()
    for  i in range(0,len(sequence)):
        el = sequence[i]
        distribution[el] +=1
    print("Distribution: {}". format(distribution.most_common()))
    print("Generated random sequence of length {} with {} symbols. Pattern {} is generated {} times".format(
                            sequence_length, no_random_symbols, [i for i in range(1,pattern_length+1)], no_instances_pattern_org))
    return sequence

def test_generate_dataset():
    sequence = generate_dataset(10,3,50,5)
    print(sequence)
    
#test_generate_dataset()