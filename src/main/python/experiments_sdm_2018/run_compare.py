from utils import save_sequences,load_text,make_clean_files_with_sentences, preprocess_text
from create_synthetic_dataset import generate_dataset
import wrapper
from wrapper.seq_pattern_mining import runFCI, runSkopus, runGoKrimp, runQCSP
from wrapper.settings import PROJECT_DIR

run_QCSP = True
run_FCI = False
run_others = True

print("Running {} and {} and {}".format("qcsp" if run_QCSP else "", \
                                        "FCI" if run_FCI else "",  \
                                        "SkOPUS and GoKRIMP" if run_others else ""))

top_k_text = 50 #For performance, set to 50, else set to 250
maxsize_text = 5
minsup=10
alpha = 2.0


def run_synth():
    #Note: has no gaps... all items are frequent
    # ======= LOAD DATA ======= 
    alphabet_size = 50
    sequence_size = 2000
    pattern_size = 5
    occurrences = 40
    sequence = generate_dataset(alphabet_size,pattern_size,sequence_size,occurrences)
    fname = PROJECT_DIR + '/data/synth_sequence.txt'  
    fname_labels = PROJECT_DIR + '/data/synth_sequence_labels.txt'   
    #For FCI/QCSP:
    save_sequences([sequence],fname) 
    #For Skopus/GoKrimp
    sequences = []
    for i in range(0,2000,1):
        sequences.append(sequence[i:i+20])
    sd_file = PROJECT_DIR + '/temp/synt_sequence_db.txt'
    fh = open(sd_file,'w')
    for s in sequences:
        fh.write(' '.join([str(i) for i in s]))
        fh.write('\n')
    fh.close()    
    
    if run_others:
        runGoKrimp(sd_file)
        runSkopus(dataset_path=sd_file, minsup=1, maxsize=maxsize_text, topK=top_k_text)
    if run_QCSP:
        runQCSP(inputFile=fname, alpha=2.0, maxsize=maxsize_text, topK=top_k_text)
    if run_FCI:
        runFCI(dataset_path=fname, minsup=1, maxsize=maxsize_text, topK=top_k_text)

def run_moby():
    # ======= LOAD DATA ======= 
    dataset_file = PROJECT_DIR + '/data/moby_dick_gutenberg.txt';
    sequences = load_text(dataset_file)
    sequences = preprocess_text(sequences, minsup=minsup)
    #For QCSP/FCI:
    total_size = 0
    single_sequence = []
    for sequence in sequences:
        single_sequence.extend(sequence)
        total_size += len(sequence)        
    print("Total length: {}".format(total_size))
    #For FCI
    sfile = PROJECT_DIR + 'data/moby_dick_gutenberg_single_preprocessed.txt'
    fh = open(sfile,'w')
    for item in  single_sequence:
        fh.write(str(item))
        fh.write(' ')
    fh.close()
    #For skopus/GoKrimp: multiple sentences
    clean_multi = make_clean_files_with_sentences(dataset_file)
                                
    #=======  MINING ======= 
    if run_others:
        runGoKrimp(clean_multi)
        runSkopus(dataset_path=clean_multi, minsup=1, maxsize=maxsize_text, topK=top_k_text)
    if run_QCSP:
        runQCSP(inputFile=sfile, alpha=2.0, maxsize=maxsize_text, topK=top_k_text)
    if run_FCI:
        #TODO: With gaps!
        runFCI(dataset_path=sfile, minsup=minsup, maxsize=maxsize_text, topK=top_k_text)

def run_jmlr():
    # ======= LOAD DATA ======= 
    dataset_file =  PROJECT_DIR + '/data/JMLR_skopus.txt';
    preprocessed_dataset_file = PROJECT_DIR + '/data/JMLR_skopus_processed.txt';
    sequences = load_text(dataset_file)
    sequences = preprocess_text(sequences, minsup=minsup, prune_sequence_based=True)
    #For QCSP/FCI:
    single_sequence = []
    for sequence in sequences:
        single_sequence.extend(sequence)  
    sfile = PROJECT_DIR + '/data/JMLR_skopus_single.txt'
    fh = open(sfile,'w')
    for item in  single_sequence:
        fh.write(str(item))
        fh.write(' ')
    fh.close()
     
    #For Skopus/GoKrimp/FCI
    save_sequences(sequences,preprocessed_dataset_file)
    #=======  MINING ======= 
    if run_others:
        runGoKrimp(preprocessed_dataset_file)
        runSkopus(dataset_path=preprocessed_dataset_file, minsup=1, maxsize=maxsize_text, topK=top_k_text)
    if run_QCSP:
        runQCSP(inputFile=sfile, alpha=2.0, maxsize=maxsize_text, topK=top_k_text)
    if run_FCI:
        #TODO: With gaps!
        runFCI(dataset_path=sfile, minsup=minsup, maxsize=maxsize_text, topK=top_k_text)
    

def run_trump():
    # ======= LOAD DATA ======= 
    dataset_file = PROJECT_DIR + '/data/trump_tweets_2016.txt';
    preprocessed_dataset_file = PROJECT_DIR + '/data/trump_tweets_2016_processed.txt';
    sequences = load_text(dataset_file)
    #remove tweet stuff, such as 'https://t.co/*' and &amp; words/people starting with @
    for sequence in sequences:
        for idx,item in enumerate(sequence):
            if item.startswith('https://'):
                sequence[idx] = None
            if item == '&amp;':
                sequence[idx] = None
            if item.startswith('@'):
                sequence[idx] = None
    sequences = preprocess_text(sequences, minsup=minsup, prune_sequence_based=True)
    #For QCSP:
    single_sequence = []
    for sequence in sequences:
        single_sequence.extend(sequence)   
    sfile = PROJECT_DIR + '/data/trump_tweets_2016_single.txt'
    fh = open(sfile,'w')
    for item in  single_sequence:
        fh.write(str(item))
        fh.write(' ')
    fh.close()
    
    #For Skopus/GoKrimp
    save_sequences(sequences, preprocessed_dataset_file)                              
    #=======  MINING ======= 
    if run_others:
        runGoKrimp(preprocessed_dataset_file)
        runSkopus(dataset_path=preprocessed_dataset_file, minsup=1, maxsize=maxsize_text, topK=top_k_text)
    if run_QCSP:
        runQCSP(inputFile=sfile, alpha=2.0, maxsize=maxsize_text, topK=top_k_text)
    if run_FCI:
        #TODO: With gaps!
        runFCI(dataset_path=sfile, minsup=minsup, maxsize=maxsize_text, topK=top_k_text)
        
run_synth()
run_moby()    
run_jmlr()
run_trump()

    