import collections, time

from utils import plot_data,  load_text, preprocess_text
from create_synthetic_dataset import generate_dataset
import wrapper
from wrapper.seq_pattern_mining import runQCSP                
from wrapper.settings import PROJECT_DIR

def experiment_figb_moby():
    # ======= PARAMETERS generations ======= 
    dataset_file = PROJECT_DIR + '/data/moby_dick_gutenberg.txt';
    # load and pre-process
    sequences = load_text(dataset_file)
    sequences = preprocess_text(sequences, minsup=10)
    
    total_size = 0
    single_sequence = []
    for sequence in sequences:
        single_sequence.extend(sequence)
        total_size += len(sequence)
        
    print("Total length: {}".format(total_size))
    fout = PROJECT_DIR + '/temp/moby_sequence.txt'
    fh = open(fout,'w')
    for item in single_sequence:
        fh.write(str(item))
        fh.write(' ')
    fh.close()
    print('Saved {}'.format(fout))               
    #=======  PARAMETERS MINING ======= 
    counter_items = collections.Counter(single_sequence)
    print("Sequence: len=%d, dict_size=%d, start=%s, most_common: %s, least_common: %s" % (len(single_sequence), len(counter_items), single_sequence[0:10], counter_items.most_common(5), counter_items.most_common()[-5:]))
    
    #=======  RUN PROJECTION-BASED DFS =======     
    alpha = 2.0
    maxsizes = [i for i in range(2,12, 1)]
    top_k_mine = 20
    
    runtimes = []
    runtimes_no_prune = []

    for maxsize in maxsizes:
        #case 3: no pruning
        start = time.time()
        runQCSP(inputFile=fout, alpha=alpha, maxsize=maxsize, topK=top_k_mine, pruningOf=True)
        end = time.time()
        runtimes_no_prune.append(end - start)
        print("Done running maxsize=%s. Elapsed: %.3f seconds." %  (maxsize, end-start)) 
        
        #case 1: top-k pruning
        start = time.time()
        runQCSP(inputFile=fout, alpha=alpha, maxsize=maxsize, topK=top_k_mine, pruningOf=False)
        end = time.time()
        runtimes.append(end - start)
        print("Done running maxsize=%s. Elapsed: %.3f seconds." %  (maxsize, end-start)) 
      
        print("=== Status === ")
        print(runtimes)   
        print(runtimes_no_prune)
        
def plot_experiment_figb_moby():
    maxsizes = [i for i in range(2,12,1)]
    #java rt
    runtimes = [1.2918310165405273, 2.9604549407958984, 7.434119939804077, 16.480681896209717, 41.21421408653259, 110.25116109848022, 348.7135729789734, 840.3128459453583, 2379.545804977417, 6781.24141907692]
    runtimes_no_prune = [0.9898009300231934, 2.0461268424987793, 4.434855937957764, 10.795655965805054, 29.595823049545288, 89.68659806251526, 337.98394417762756, 1197.409080028534, 3644.7672159671783, 11802.843724012375]
    
    maxval = 13000.0
    plot_data(
        {'values': maxsizes, 'label': 'maxsize', 'label_axis':'maxsize',
         'ticks': [1,2,3,4,5,6,7,8,9,10,11], 'min':1,'max':12},
        {'values': runtimes, 'label': 'top k', 'label_axis': 'log runtime (sec)','log_scale': True, 
         #'ticks': ['1','10','100','1000','10000'],
          'min':0, 'max': maxval},
        {'values': runtimes_no_prune, 'label': 'no pruning', 'label_axis': 'log runtime (sec)','log_scale': True,
         #'ticks': ['1','10','100','1000','10000'],
          'min':0, 'max': maxval},
        None,
        'perf_moby_figb_rt.png')
    
def plot_experiment_figb_moby_v2():
    maxsizes = [i for i in range(2,12,1)]
   
    leaves = [112355, 418301, 1165104, 2966852, 7413329, 18946575, 48183944, 125354013, 330272220, 873250169]
    leaves_no_prune=  [155646, 759528, 2817579, 9624389, 31950576, 104912317, 343932290, 1127858852, 3700787831, 12149560276]

    maxval = 15000000000
    plot_data(
        {'values': maxsizes, 'label': 'maxsize', 'label_axis':'maxsize',
         'ticks': [1,2,3,4,5,6,7,8,9,10,11], 'min':1,'max':12},
        {'values': leaves, 'label': 'top k', 'label_axis': '#candidates (log scale)', 'log_scale': True, 
         #'ticks': ['1','10','100','1000','10000'],
          'min':0, 'max': maxval},
        {'values': leaves_no_prune, 'label': 'no pruning', 'label_axis': '#candidates (log scale)','log_scale': True, 
         #'ticks': ['1','10','100','1000','10000'],
          'min':0, 'max': maxval},
        None,
        'perf_moby_figb_candidates.png')

'''
Voor QCSP A, en A QCSP pruning:\\
Figure a: plot runtime (Y-as), X-as: maxsize=2 tot 50. Synthetic\\
'''
def experiment_figa_synth():
    # ======= PARAMETERS generations ======= 
    alphabet_size = 50
    sequence_size = 2000
    pattern_size = 5
    occurrences = 40
    sequence = generate_dataset(alphabet_size,pattern_size,sequence_size,occurrences)
    fout = PROJECT_DIR + '/temp/sequence.txt'
    fh = open(fout,'w')
    for item in sequence:
        fh.write(str(item))
        fh.write(' ')
    fh.close()
    print('Saved {}'.format(fout))
    
    #=======  PARAMETERS MINING ======= 
    alpha = 2.0 #was 1.0
    top_k_mine = 5
    maxsizes = [i for i in range(2,12, 1)]
    runtimes = []
    runtimes_no_prune = []
    for maxsize in maxsizes:
        #case 3: no pruning
        start = time.time()
        runQCSP(inputFile=fout, alpha=alpha, maxsize=maxsize, topK=top_k_mine, pruningOf=True)
        end = time.time()
        runtimes_no_prune.append(end - start)
        print("Done running maxsize=%s. Elapsed: %.3f seconds." %  (maxsize, end-start)) 
        
        #case 1: top-k pruning
        start = time.time()
        runQCSP(inputFile=fout, alpha=alpha, maxsize=maxsize, topK=top_k_mine, pruningOf=False)
        end = time.time()
        runtimes.append(end - start)
        print("Done running maxsize=%s. Elapsed: %.3f seconds." %  (maxsize, end-start)) 
      
        print("=== Status === ")
        print(runtimes)   
        print(runtimes_no_prune)   
      
def plot_experiment_figa_synth():
    maxsizes = [i for i in range(2,12, 1)]
    
    runtimes =  [0.28031301498413086, 0.3823051452636719, 0.5872390270233154, 1.1037788391113281, 2.901305913925171, 9.339435815811157, 32.823801040649414, 123.71828413009644, 476.5977940559387, 1880.5794229507446]
    runtimes_no_prune = [0.22762489318847656, 0.3309781551361084, 0.4437539577484131, 0.8427779674530029, 2.0811679363250732, 7.129680871963501, 27.753065824508667, 115.57657408714294, 481.28915905952454, 2026.2348430156708]

    maxval = 14000.0
    plot_data(
        {'values': maxsizes, 'label': 'maxsize', 'label_axis':'maxsize',
         'ticks': list(range(1,12)), 'min':1,'max':12},
        {'values': runtimes, 'label': 'top k', 'label_axis': 'log runtime (sec)', 'log_scale': True, 
         #'ticks': ['1','10','100','1000','10000'],
          'min':0, 'max': maxval},
        {'values': runtimes_no_prune, 'label': 'no pruning', 'label_axis': 'log runtime (sec)', 'log_scale': True,
         #'ticks': ['1','10','100','1000','10000'],
          'min':0, 'max': maxval},
         None,
        'perf_synth_figa_rt.png')

def plot_experiment_figa_synth_v2():
    maxsizes = [i for i in range(2,12, 1)]

    leaves=[1359, 9991, 48409, 197743, 695255, 2200132, 6143749, 16866718, 48685687, 155524296]
    leaves_no_prune=[2205, 19158, 97085, 421438, 1727061, 6902524, 27273893, 107258318, 420842984, 1650020563]
    
    maxval = 2000000000
    plot_data(
        {'values': maxsizes, 'label': 'maxsize', 'label_axis':'maxsize',
         'ticks': list(range(1,12)), 'min':1,'max':12},
        {'values': leaves, 'label': 'top k', 'label_axis': '#candidates (log scale)', 'log_scale': True, 
         #'ticks': ['1','10','100','1000','10000'],
          'min':0, 'max': maxval},
        {'values': leaves_no_prune, 'label': 'no pruning', 'label_axis': '#candidates (log scale)', 'log_scale': True,
         #'ticks': ['1','10','100','1000','10000'],
          'min':0, 'max': maxval},
        None,
        'perf_synth_figa_candidates.png')


def experiment_figc_moby():
    # ======= PARAMETERS generations ======= 
    dataset_file = PROJECT_DIR + '/data/moby_dick_gutenberg.txt';
    # load and pre-process
    sequences = load_text(dataset_file)
    sequences = preprocess_text(sequences, minsup=10)
    
    total_size = 0
    single_sequence = []
    for sequence in sequences:
        single_sequence.extend(sequence)
        total_size += len(sequence)
        
    print("Total length: {}".format(total_size))
    fout = PROJECT_DIR + '/temp/moby_sequence.txt'
    fh = open(fout,'w')
    for item in single_sequence:
        fh.write(str(item))
        fh.write(' ')
    fh.close()                   
    #=======  PARAMETERS MINING ======= 
    counter_items = collections.Counter(single_sequence)
    print("Sequence: len=%d, dict_size=%d, start=%s, most_common: %s, least_common: %s" % (len(single_sequence), len(counter_items), single_sequence[0:10], counter_items.most_common(5), counter_items.most_common()[-5:]))
    
    #=======  RUN PROJECTION-BASED DFS =======     
    alpha = 2.0
    maxsize = 5
    top_k_mines = range(1,10000,1000)
    runtimes = []
    
    for topk in top_k_mines:
        #case 1: perfect pruning
        start = time.time()
        runQCSP(inputFile=fout, alpha=alpha, maxsize=maxsize, topK=topk)
        end = time.time()
        runtimes.append(end - start)
        print("Done running maxsize=%s. Elapsed: %.3f seconds." %  (maxsize, end-start)) 
        #Done running maxsize=16. Elapsed: 367.380 seconds.
        print("=== Status === ")
        print(runtimes)
    
def plot_experiment_figc_moby():
    top_k_mines = range(1,10000,1000)
    runtimes =   [4.53348183631897, 24.530012130737305, 24.007652044296265, 24.52393388748169, 24.99486494064331, 25.826712131500244, 24.05154013633728, 24.448649883270264, 24.486686944961548, 25.467614889144897]
    maxval = 26
    plot_data(
        {'values': top_k_mines, 'label': 'top-k', 'label_axis':'top-k',
         'ticks': [1,2500,5000,7500,10000], 'min':1,'max':10000},
        {'values': runtimes, 'label': 'runtime min_coh=1.0', 'label_axis': 'runtime (sec)', 
         #'ticks': ['1','10','100','1000','10000'],
          'min':0, 'max': maxval},
        None,None,
        'perf_mobyfigc.png')
    
def parse_log_for_leaves():
    file = 'run_prune_performance.log'
    f = open(file,'r')
    structure1 = [[],[]]
    structure2 = [[],[]]
    i = -1
    j = 0
    structure = structure1
    for line in f:
        if 'Parameters' in line:
            print(line.strip())
            i = (i + 1) % 2
            j +=1
        if 'Leaves' in line:
            print(line.strip())   
            structure[i].append(int(line.strip().split(':')[1]))
        if j == 10 * 2 + 1:
            structure = structure2
    f.close()
    print("Leaves status moby")
    for row in structure1:
        print(row)
    print("Leaves status synth")
    for row in structure2:
        print(row)

# ====== RUN ======
experiment_figb_moby()
plot_experiment_figb_moby()
parse_log_for_leaves()
plot_experiment_figb_moby_v2()

experiment_figa_synth()
plot_experiment_figa_synth()
plot_experiment_figa_synth_v2()

experiment_figc_moby()
plot_experiment_figc_moby()

