import os,re,collections,time
from stemming.porter2 import stem
import numpy as np
import codecs

def load_text(dataset):
    #f = open(dataset)
    #content = f.readlines()
    f = codecs.open(dataset,'r', encoding='UTF-8')
    content = f.readlines()
    sequences = [] 
    print('Found {} lines (or sequences) in original text'.format(len(content)))
    for line in content:
        sequence = line.split()
        sequence = [item.strip() for item in sequence]
        sequence = [item for item in sequence if item != '']
        sequences.append(sequence)
    f.close()
    print('Found {} lines (or sequences) in text'.format(len(sequences)))
    print('Average sequence length is: {}'.format(np.mean([len(s) for s in sequences])))
    return sequences

def preprocess_text(sequences, minsup=10, prune_sequence_based=False):
    stopwords_txt = os.path.join(os.path.dirname(__file__), './stop_words.txt')
    f_stopwords = open(stopwords_txt)
    stopwords_lst = set(f_stopwords.read().split())
    f_stopwords.close()
    new_sequences = []
    #Bug: after removing '-' from token, such as snow-white -> add token snow,white to sequence
    for idx, sequence in enumerate(sequences):
        sequence = [re.sub(r'\W+', ' ', word)  if word != None else None for word in sequence] 
        new_sequence = []
        for token in sequence:
            if token is None:
                new_sequence.append(token)
            elif len(token.split()) == 1:
                new_sequence.append(token)
            else:
                for tokensub in token.split():
                    new_sequence.append(tokensub)
        sequences[idx] = new_sequence
    for sequence in sequences:
        sequence = [word.lower() if word != None else None for word in sequence] 
        sequence = [word.replace('\'','') if word != None else None for word in sequence] 
        sequence = [word for word in sequence if word not in stopwords_lst ] #remove stopwords, do not keep space
        sequence = [stem(word) if word != None else None for word in sequence] 
        sequence = [word.strip() if word != None else None for word in sequence] 
        sequence = [None if word == '' else word for word in sequence]
        new_sequences.append(sequence)
    #prune... not count occurrence of word once for each abstract...
    counter = collections.Counter()
    if prune_sequence_based:
        for sequence in new_sequences:
            set_sequences = set()
            for item in sequence:
                set_sequences.add(item)
            for item in set_sequences:
                counter[item]+=1   
        print('Found #{} distinct items (counting sequence based)'.format(len(counter)))
        del counter[None]
    else:
        for sequence in new_sequences:
            for item in sequence:
                counter[item]+=1    
        print('Found #{} distinct items'.format(len(counter)))
        del counter[None]
    #on minup
    for sequence in new_sequences:
        for idx,item in enumerate(sequence):
            if counter[item] < minsup:
                sequence[idx] = None #keep none if minsup small
    return new_sequences

def test():
    #bug Boris
    print("test:")
    sequences =  preprocess_text([["snow-white", "snow","white", "try", "works", "try-works"]],minsup=1)
    print(sequences)

test()


def save_sequences(sequences, fout):
    fh = open(fout,'w')
    for sequence in sequences:
        line = ' '.join([str(item) for item in sequence if item != None])
        fh.write(line)
        fh.write('\n')
    fh.close()
    print('Saved {}'.format(fout))
    
def save_sequences_with_gaps(sequences, fout):
    fh = open(fout,'w')
    for sequence in sequences:
        line = ' '.join([str(item) if item != None else str(-1) for item in sequence])
        fh.write(line)
        fh.write('\n')
    fh.close()
    print('Saved {}'.format(fout))
     
def make_clean_files_with_sentences(fin):
    sequences = load_text(fin)
    sequences_single = []
    for sequence in sequences:
        sequences_single.extend(sequence)    
    sentences = [] 
    last_match = 0
    for idx,item in enumerate(sequences_single):
        if '?' in item or '!' in item or '.' in item:
            sentences.append(sequences_single[last_match:idx+1])
            last_match = idx + 1
    sequences = preprocess_text(sentences, 2)
    fin2 = os.path.join(os.path.dirname(fin), os.path.basename(fin) + '_sentences_clean.txt')
    fh = open(fin2,'w')
    for sequence in sequences:
        line = ' '.join([item for item in sequence if item != None])
        fh.write(line)
        fh.write('\n')
    fh.close()
    print('Saved sentences {}'.format(fin2))
    print("Number of sentences is {}. Average length is {}.".format(len(sequences), np.mean([len(sentence) for sentence in sequences])))
    return fin2

import matplotlib
import matplotlib.pyplot as plt
import os.path
import numpy as np


#
# Assuming each series is a dict with: 'values', 'min','max','label','log_scale'
#
def plot_data(seriesx, seriesyleft, seriesyright, serieszright, filename_fig):
    #See http://www.randalolson.com/2014/06/28/how-to-make-beautiful-data-visualizations-in-python-with-matplotlib/

    # You typically want your plot to be ~1.33x wider than tall. This plot is a rare
    # exception because of the number of lines being plotted on it.
    # Common sizes: (10, 7.5) and (12, 9)
    myfontsize = 13
    plt.figure(figsize=(7, 4),dpi=100)

    # Remove the plot frame lines. They are unnecessary chartjunk.
    fig, ax1 = plt.subplots()

    # Ensure that the axis ticks only show up on the bottom and left of the plot.
    # Ticks on the right and top of the plot are generally unnecessary chartjunk.
    ax1.get_xaxis().tick_bottom()
    ax1.get_yaxis().tick_left()
    ax1.spines["top"].set_visible(False)
    ax1.spines["right"].set_visible(False)
    ax1.spines["bottom"].set_visible(True)
    ax1.spines["bottom"].set_color('#aaaaaa')
    ax1.spines["bottom"].set_linestyle(':')
    ax1.spines["left"].set_visible(True)
    ax1.spines["left"].set_color('#aaaaaa')
    ax1.spines["left"].set_linestyle(':')
    if 'log_scale' in seriesyleft:
        ax1.set_yscale('log')
    ax1.set_xlabel(seriesx['label_axis'],fontsize=myfontsize+1)
    ax1.set_ylabel(seriesyleft['label_axis'],fontsize=myfontsize+1)
    if 'log_scale' in seriesx:
        ax1.set_xscale('log')

    # Limit the range of the plot to only where the data is.
    # Avoid unnecessary whitespace.
    xmin = seriesx['values'][0]
    if 'min' in seriesx:
        xmin = seriesx['min'] #+ _some_margin_axis(seriesx['values'],True)
    xmax = seriesx['values'][-1]
    if 'max' in seriesx:
        xmax = seriesx['max'] #+ _some_margin_axis(seriesx['values'],False)
    plt.xlim(xmin,xmax)
    print("plot limits %s x axis: [%.5f,%5.f]" % (seriesx['values'],xmin,xmax))

    #See http://stackoverflow.com/questions/14530113/set-ticks-with-logarithmic-scale
    #step_x = max(len(seriesx['values'])/10,1)
    if 'ticks' in seriesx:
       if 'tick_labels' in seriesx:
           ax1.set_xticks(seriesx['ticks'])
           ax1.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
           ticks=ax1.get_xticks().tolist()
           for i in range(0,len(ticks)):
               ticks[i] = seriesx['tick_labels'][i]
           ax1.set_xticklabels(ticks)
       else:
           ax1.set_xticks(seriesx['ticks'])
           ax1.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())

    if 'ticks' in seriesyleft:
        ax1.set_yticks(seriesyleft['ticks'])
        ax1.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
   
    # Remove the tick marks; they are unnecessary with the tick lines we just plotted.
    ax1.tick_params(axis="both", which="both", bottom="off", top="off",
                    labelbottom="on", left="off", right="off", labelleft="on", labelsize=myfontsize-2)
    line_left, = ax1.plot(seriesx['values'],
            seriesyleft['values'],
            "-",
            lw=1.0, color=(0.2,0.5,0.7), marker='x',
            label= seriesyleft['label'])
    ymin_left = seriesyleft['values'][0] + _some_margin_axis(seriesyleft['values'],True)
    if 'min' in seriesyleft:
        ymin_left = seriesyleft['min']
    ymax_left = seriesyleft['values'][-1]  + _some_margin_axis(seriesyleft['values'],True)
    if 'max' in seriesyleft:
        ymax_left = seriesyleft['max']
    ax1.set_ylim(ymin=ymin_left, ymax=ymax_left)

    if seriesyright: 
        line_right, = ax1.plot(seriesx['values'],
                seriesyright['values'],
                '--',
                lw=1.0, color=(1.0,0.5,0.1), marker='o',
                label=seriesyright['label']
                )
    if serieszright:
        line_right2, = ax1.plot(seriesx['values'],
                serieszright['values'],
                '--',
                lw=1.0, color=(0.18,0.62,0.17), marker='d',
                label=seriesyright['label']
                )
         
        ymin_right = seriesyright['values'][0]  + _some_margin_axis(seriesyright['values'],True)
        if 'min' in seriesyright:
            ymin_right = seriesyright['min']
        ymax_right = seriesyright['values'][-1] + _some_margin_axis(seriesyright['values'],False)
        if 'max' in seriesyright:
            ymax_right = seriesyright['max']
        #ax2.set_ylim(ymin=ymin_right,ymax=ymax_right)
    
    if serieszright:
        plt.legend((line_left, line_right, line_right2),(seriesyleft['label'], seriesyright['label'], serieszright['label']), loc='upper left', prop={'size':10})

    if seriesyright:
        plt.legend((line_left, line_right),(seriesyleft['label'], seriesyright['label']), loc='upper left', prop={'size':10})


    fig.set_size_inches(5.5,4)
    plt.gcf().subplots_adjust(bottom=0.05)
    plt.tight_layout()
    fig.savefig(filename_fig, dpi=100)
    print("Saved %s" % filename_fig)


def _some_margin_axis(list, left):
    margin = np.std(list) * 2
    if left:
        if list[0] < list[-1]: #ascending
            return - margin
        else:
            return  margin
    else:
        print("%s -> %.3f" % (list,margin))
        if list[0] < list[-1]:  #ascending
            return margin
        else:
            return -margin
        
'''
Show original text fragment in html, with color-coding of items/stopwords and infrequent items
Show top-k patterns hightlighted in HTML 

Author: Len Feremans
'''
from stemming.porter2 import stem

def clamp_rgb(r, g, b): #see also https://stackoverflow.com/questions/141855/programmatically-lighten-a-color
    return min(255, int(r)), min(255, int(g)), min(255, int(b))

def make_color_palette(base_color, amount):
    import numpy as np
    rng = np.arange(1.0, 2.0, 1.0/float(amount))
    color_palette = list()
    for x in rng:
       c = clamp_rgb(base_color[0] * x, base_color[1] * x, base_color[2] * x)
       c_hex = '#{:02X}{:02X}{:02X}'.format(c[0],c[1],c[2])
       color_palette.append(c_hex)
    return color_palette

def save_to_html(sequence_orig, sequence_filtered, counter_items, output_filename, patterns, stopwords_lst):
    html_content = \
"""<html>
<head>
<script>
function toggleIt(){
   for(var j=0; j < arguments.length; j++)
   {
      var elements = document.getElementsByClassName(arguments[j]); 
      for(var i=0; i < elements.length; i++){  
          elements[i].classList.toggle('focus');
      };
   }
}
</script>
<style>body {
  background-color: #FFFFFF;
}
.stopword {
   color: #C4C4C4;
}
.lowsupport {
   font-style: italic;
   color: #A4A4A4;
}
"""
    #color: blueish color for normal items
    #base_color = (181, 226, 255)
    #color_palette_items = make_color_palette(base_color, len(counter_items))
    #for index, itemandsupp in enumerate(counter_items.most_common(len(counter_items))):
    #    html_content += ".%s {\n   background-color: %s\n}\n" % (itemandsupp[0],  color_palette_items[index])
    #should be last, so priority in background-color
    html_content += '''.focus{
   background-color: #FDE4AE;
}
</style></head><body>'''
    #save sequence
    for index, word in enumerate(sequence_orig):
        if word is None or word == '':
             html_content += '&nbsp;&nbsp;'
        if sequence_filtered[index] != None: #regular item
             html_content += '<span class="%s">%s</span> ' % (stem(word),word)
        elif word in stopwords_lst: #stopword
             html_content += '<span class="stopword">%s</span> ' % (word)
        else: #assuming low support
             html_content += '<span class="lowsupport">%s</span> ' % (word)
        if index % 20 == 0:
            html_content += '\n'
    #Show patterns
    html_content += '\n<table><tr><th>Pattern</th><th>Q-Cohesion</th><th>Support</th></tr>\n'
    for pattern in patterns:
        p_id = 'p' + ''.join(pattern[1])
        p_label = ' '.join(pattern[1])
        p_args =  '\',\''.join(pattern[1])
        support = pattern[2] if len(pattern)>2 else -1
        html_content += '<tr><td><a class="%s" href="#" onclick="toggleIt(\'%s\',\'%s\'); return false;">%s</a></td><td>%.3f</td><td>%d</td></tr>\n' \
                                    % (p_id, p_id, p_args,p_label, pattern[0],support)
        #e.g. <a class="pmobydick" href="#" onclick="toggleIt('pmobydick','moby','dick'); return false;">moby dick</a></td>
    #save footer:
    html_content += '\n</table></body></html>'
    f_out = open(output_filename,'w')
    f_out.write(html_content)
    f_out.close()
    print("Saved %s" % output_filename)
