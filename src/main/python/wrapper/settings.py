'''
SETTINGS FILE FOR CALLING QCSP and related work from PYTHON
'''
import os
import os.path
from sys import stderr
from os.path import expanduser
home = expanduser("~")
PROJECT_DIR = home + '/git2/qcsp_public/'
PATH_QCSP = PROJECT_DIR + '/target/QCSP-pub-1.0.0.jar' #build using mvn clean install
PATH_FCI = PROJECT_DIR + '/lib/sequence-pattern-mining-clean-0.1.0.jar'
PATH_SKOPUS = PROJECT_DIR + '/lib/skopus.jar'
PATH_GOKRIMP = PROJECT_DIR + '/lib/spmf.jar'

if os.path.isdir(PROJECT_DIR) and os.path.isfile(PATH_QCSP):
    print("OK: Found QCSP java library" + PATH_QCSP)
else:
    stderr.write("ERROR: QCSP java library not found!\n Run \"mvn clean install\" to build " + PATH_QCSP + ".\nAlso check src/main/python/wrapper/settings.py and refer to existing directories.\n")
    
if os.path.isdir(PROJECT_DIR) and os.path.isfile(PATH_FCI) and os.path.isfile(PATH_SKOPUS) and os.path.isfile(PATH_GOKRIMP):
    print("OK: Found  FCI/Skopus/GOKRIMP java libraries\n")
else:
    stderr.write("ERROR: FCI/Skopus/GOKRIMP java libraries not found. \nPlease change src/main/python/wrapper/settings.py and refer to existing directories. \nPROJECT_DIR=" + PROJECT_DIR + "\n")
    