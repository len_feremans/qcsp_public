from seq_pattern_mining import runQCSP, runFCI, runSkopus, runGoKrimp
from settings import PROJECT_DIR

def test_QCSP():
    print("TESTING QCSP ON MOBY FRAGMENT")
    runQCSP(PROJECT_DIR + '/data/moby_dick_gutenberg_sentences_clean.txt', 2.0, 5, 100)
    
    
def test_FCI():
    print("TESTING FCI ON MOBY FRAGMENT")
    runFCI( PROJECT_DIR + '/data/moby_dick_gutenberg_sentences_clean.txt', 20, 10, 10)

def test_SKOPUS():
    print("TESTING SKOPUS ON MOBY FRAGMENT")
    runSkopus(PROJECT_DIR + '/data/moby_dick_gutenberg_sentences_clean.txt', 2, 10, 10)
        
def test_GoKrimp():
    print("TESTING GoKRIMP ON MOBY FRAGMENT")
    runGoKrimp(PROJECT_DIR + '/data/moby_dick_gutenberg_sentences_clean.txt')

test_QCSP()
test_FCI()
test_SKOPUS()
test_GoKrimp()