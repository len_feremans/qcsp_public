#Author: Len Feremans
#Wrapper running java-based sequential pattern mining implemented in java
#Allows to run 
# >GoGrimp (SPMF library), 
# > Skopus (github Library Francois Petitjean)
# > FCI (bitbucket Len Feremans)
# > QCSP (bitbucket Len Feremans)
import subprocess

from wrapper.settings import PATH_QCSP, PATH_FCI, PATH_GOKRIMP, PATH_SKOPUS

def _bool2str(b):
    return "true" if b else "false"
    
def runQCSP(inputFile,alpha,maxsize,topK,pruningOf=False):
    classpath = [PATH_QCSP,PATH_FCI]
    command = ["java", "-cp", ":".join(classpath), "core.QCSP",  inputFile, str(alpha),  str(maxsize), str(topK), _bool2str(pruningOf), "-Xmx4G"]
    print(" ".join(command))
    subprocess.call(command, timeout=1000*60*6) #default: 6 hour time-out (if it works)

def runFCI(dataset_path, minsup, maxsize, topK):
    classpath = [PATH_QCSP,PATH_FCI]
    command = ["java", "-cp", ":".join(classpath), "wrappers.RunFCI",  dataset_path, str(minsup),  str(maxsize), str(topK),"-Xmx4G"]
    print(" ".join(command))
    subprocess.call(command)

def runSkopus(dataset_path, minsup, maxsize, topK):
    classpath = [PATH_QCSP,PATH_SKOPUS]
    command = ["java", "-cp", ":".join(classpath), "wrappers.RunSkopus",  dataset_path, str(maxsize), str(topK),"-Xmx4G"]
    print(" ".join(command))
    subprocess.call(command)
    
def runGoKrimp(dataset_path):
    classpath = [PATH_QCSP,PATH_FCI,PATH_GOKRIMP]
    command = ["java", "-cp", ":".join(classpath), "wrappers.RunGoKrimp",  dataset_path,"-Xmx4G"]
    print(" ".join(command))
    subprocess.call(command)
    
