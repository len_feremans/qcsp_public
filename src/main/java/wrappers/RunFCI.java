package wrappers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import be.uantwerpen.pattern_mining.MineCoOccuringItems;
import util.DatLabFileToNormalFile;
import util.Timer;
import util.Utils;

public class RunFCI {
	
	public static void main(String[] args) throws Exception {
		runFCI(new File(args[0]), Integer.valueOf(args[1]), Integer.valueOf(args[2]), Integer.valueOf(args[3]));
	}
	/*
	 * For \textsc{FCI} we set $maxsize = 10$. Since we can not directly control the number of patterns, we decrease $min\_coh$ until 
at least $k$ patterns are reported, any only report the runtime of the run with this lowest value of $min\_coh$. 
	 */
	public static void runFCI(File dataset, int minsup, int maxsize, int topK) throws Exception {
		File datasetLab = new File(dataset.getParentFile(), dataset.getName() + ".lab");
		File datasetSeq = new File(dataset.getParentFile(), dataset.getName() + ".seq");
		DatLabFileToNormalFile.convertToDataLab(dataset, datasetSeq, datasetLab);
		double[] minCohs = new double[] {0.9, 0.5, 0.25, 0.1, 0.05, 0.028, 0.02, 0.015, 0.01};
		for(double minCohesion : minCohs ) {
			Object[] result= runSingle(datasetLab, datasetSeq, minCohesion, minsup, maxsize);
			long elapsed = (Long)result[0];
			long noPatterns = (Long)result[1];
			if(noPatterns >= topK || minCohesion == 0.01) {
				File patterns = (File)result[2];
				Utils.printHead(patterns,100);
				System.out.println("");
				System.out.println("Closed cohesive patterns:");
				//post-process: only closed patterns?
				showMaximal(patterns,100);
				System.out.println("FCI: mincohesion=" + minCohesion + " return k= " + noPatterns + " patterns in " + elapsed + "ms");
				break;
			}
			System.out.println("FCI: mincohesion=" + minCohesion + " return k= " + noPatterns + " patterns in " + elapsed + "ms");
		}
	}
	
	private static Object[] runSingle(File datasetLab, File datasetSeq, double minCohesion, int minsup, int maxsize) throws Exception {
		Timer timer = new Timer("runFCI " + datasetLab.getName() + " minCoh=" + minCohesion);
		MineCoOccuringItems service = new MineCoOccuringItems(datasetLab,datasetSeq,minsup);
		service.minePatterns(maxsize, minCohesion);
		long elapsed = timer.end();
		File sorted = Utils.sortCSVFile(service.getOutput(),3);
		return new Object[] {elapsed, Utils.countLines(service.getOutput()), sorted};
	}
	
	public static void showMaximal(File patterns, int count) throws IOException {
		List<String> lines = Utils.readFileUntil(patterns, count);
		for(String line: lines) {
			boolean closed = false;
			for(String line2: lines) {
				closed = closed || isMaximal(line,line2);
			}
			if(closed) {
				System.out.print("X ");
			}
			System.out.println(line);
		}
	}
	
	//closed == no super-set with higher cohesion
	private static boolean isMaximal(String line1, String line2){
		String[] pattern1 = line1.split(";")[0].split(" ");
		String[] pattern2 = line2.split(";")[0].split(" ");
		if(line1.equals(line2))
			return false;
		for(String token1 : pattern1) {
			boolean found = false;
			for(String token2 : pattern2) {
				if(token2.equals(token1))
					found=true;
			}
			if(!found)
				return false;
		}
		return true;
	}
}

