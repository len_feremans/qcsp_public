package wrappers;

import java.io.File;

import ca.pfv.spmf.algorithms.sequentialpatterns.goKrimp.AlgoGoKrimp;
import ca.pfv.spmf.algorithms.sequentialpatterns.goKrimp.DataReader;
import core.PreProcessing;
public class RunGoKrimp {


	public static void main(String[] args) throws Exception {
		runGOKrimp(new File(args[0]));
	}

	public static void runGOKrimp(File dataset) throws Exception {
		File datasetSeq = new File("./temp/" + dataset.getName() + "_spmf.seq");
		File datasetLab = new File("./temp/" + dataset.getName() + "_spmf.lab");
		//File output = new File("/Users/lfereman/git2/IQRSequentialPatternMining/temp/" + dataset.getName() + "_gokrimp.txt");
		PreProcessing.convertToDataLabMultipleSequencesSPMF(dataset, datasetSeq, datasetLab);
		DataReader reader = new DataReader();
		AlgoGoKrimp algo = reader.readData_SPMF(datasetSeq.getAbsolutePath(),datasetLab.getAbsolutePath());
		//algo.setOutputFilePath(output.getAbsolutePath());
		System.out.println("Running GoKrimp");
		algo.gokrimp();
		System.out.println("End GoKrimp");
		//algo.seqkrimp();
	}
}
