package wrappers;

import java.io.File;

import skopus.InterestingnessMeasureMain;

public class RunSkopus {

	public static void main(String[] args) throws Exception {
		runSkopus(new File(args[0]), Integer.valueOf(args[1]), Long.valueOf(args[2]));
	}
	
	public static void runSkopus(File dataset, int maxsize, long top_k) throws Exception {
		String[] args_jmlr = new String[] {dataset.getAbsolutePath(), 
						new File(dataset.getParentFile(), dataset.getName() + "_output_skopus.txt").getAbsolutePath(), 
						"-i2", "-m1","-k"+top_k,"-l"+maxsize};
		InterestingnessMeasureMain.main(args_jmlr);
	}
}
