package core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;

import util.CountMap;
import util.FileStream;
import util.IOUtils;
import util.ListMap;
import util.Pair;
import util.Timer;
import util.Triple;
import util.Utils;

/**
 * Implementation supporting sparse itemset sequences, encoded as sparse sequence with timestamps
 * 
 * @author lfereman
 */
public class QCSP_Sparse {

	public static void main(String[] args) throws Exception {
		QCSP_Sparse algo = new QCSP_Sparse(new File(args[0]), new File(args[1]), Long.valueOf(args[2]), Integer.valueOf(args[3]), Integer.valueOf(args[4]));
		if(args.length >= 6) {
			boolean pruningOf = Boolean.valueOf(args[5]);
			algo.pruningOf = pruningOf;
		}
		algo.run(true);
	}

	private static SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public class SequenceItem{
		private Date timestamp; 
		private Set<Integer> items;

		public SequenceItem(Date timestamp, Set<Integer> items) {
			super();
			this.timestamp = timestamp;
			this.items = items;
		}
		public Date getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(Date timestamp) {
			this.timestamp = timestamp;
		}
		public Set<Integer> getItems() {
			return items;
		}
		public void setItems(Set<Integer> items) {
			this.items = items;
		}

		//Str like 2015-01-29 10:42,0,1,2,3,4,5,6,7,8,9,10,11,12,13		
		public SequenceItem(String str) {
			String[] tokens = str.split(",");
			Date d = null;
			try {
				d = parser.parse(tokens[0]);
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
			Set<Integer> ints = new HashSet<Integer>();
			for(int i=1; i<tokens.length; i++) {
				ints.add(Integer.parseInt(tokens[i]));
			}
			this.timestamp = d;
			this.items = ints;
			//return new SequenceItem(d, ints);	
		}

		public String toString() {
			String reprItemset = "(";
			for(Integer item: this.items) {
				reprItemset += labelsList.get(item);
				reprItemset += ","; 
			}
			reprItemset = reprItemset.substring(0, reprItemset.length()-1) + ")";
			return reprItemset;
		}
	}

	//data
	private List<SequenceItem> sequenceList; 
	private List<String> labelsList;
	private CountMap<Integer> support;
	private List<Integer> itemsSortedOnAscendingSupport;
	private ListMap<Integer,Integer> projection1;

	private File inputSequence;
	private File inputLabels;

	//parameters
	long alphaSeconds; //e.g. normally, do projection of alpha * maxsize (e.g. take windows of maximal size 2.0 * 3)... for sparse time-based datasets,
	//we take projections of event that are at most alphaSecond * maxsize apart 
	int maxsize; 
	int topK;
	public boolean pruningOf = false;
	public static long DEBUG_LOOP_COUNT = 1000000;

	/**
	 * Note: gaps should be encoded as "None" or "null"
	 * 
	 * @param sequence
	 * @param labels
	 * @param alpha
	 * @param maxsize
	 * @param topK
	 * @throws IOException
	 */
	public QCSP_Sparse(File itemSetSequence, File labels, long alphaSeconds, int maxsize, int topK) throws IOException {
		//parameters
		this.inputSequence = itemSetSequence;
		this.inputLabels = labels;
		this.alphaSeconds = alphaSeconds;
		this.maxsize = maxsize;
		this.topK = topK;
		//read data
		loadData(itemSetSequence,labels);
		return;
	}

	private class Window{
		public int t;
		public int a;
		public int b;
		public Window(int t, int a, int b) {
			this.t= t; this.a=a; this.b=b;
		}
	}

	private class SPattern{
		public List<Integer> pattern;
		public SPattern() {
			this.pattern = new ArrayList<Integer>();
		}
		public SPattern(SPattern prefix, Integer item){
			this.pattern = new ArrayList<Integer>(prefix.length() + 1);
			for(int i=0; i<prefix.length();i++) {
				this.pattern.add(prefix.pattern.get(i));
			}
			this.pattern.add(item);
		}
		public int length() {
			return pattern.size();
		}
	}

	private List<Window> init = makeList(new Window(-1,-1,-1));

	public File run(boolean debug) throws IOException {
		Timer timer = new Timer("QSCP.run()");
		System.out.format("Parameters: alphaSeconds=%d, maxsize=%d, top-k=%d, pruningOf=%s\n", 
				this.alphaSeconds, this.maxsize, this.topK, this.pruningOf);
		PriorityQueue<Pair<SPattern,Double>> heapPatterns = new PriorityQueue<>(topK, heapComparator);
		double mincoh = 0.0;
		ArrayList<Triple<SPattern, List<Window>, List<Integer>>> stack = new ArrayList<>();
		stack.add(new Triple<>(new SPattern(),init,itemsSortedOnAscendingSupport));
		long iterations = 0;
		while(!stack.isEmpty()) {
			iterations++;
			Triple<SPattern, List<Window>, List<Integer>> top = stack.remove(stack.size()-1);
			SPattern X = top.getFirst();
			List<Window> P = top.getSecond();
			List<Integer> Y = top.getThirth();
			if(debug && iterations % DEBUG_LOOP_COUNT == 0)
			{
				System.out.format("First item: %s (%d/%d), ", X.pattern.size() > 0? toString(makeList(X.pattern.get(0))): "0", X.pattern.size() > 0?itemsSortedOnAscendingSupport.indexOf(X.pattern.get(0)):-1, itemsSortedOnAscendingSupport.size());
				System.out.format("Iterations:%-10d, #Patterns: %d, worst: %s, min_coh:%.3f\n", iterations/DEBUG_LOOP_COUNT, heapPatterns.size(), heapPatterns.size()>0?toString(heapPatterns.peek().getFirst().pattern):"None", mincoh); 
				System.out.format("X:%-10s,|Y|:%d,P:%d\n\n", toString(X.pattern),Y.size(), P.size());
			}
			if(Y.size() == 0) { //leaf
				if(X.length() > 1) {
					double qcoh = quantileCohesionComputedOnProjection(X,P);
					if(heapPatterns.size() < topK) {
						heapPatterns.add(new Pair<SPattern,Double>(X,qcoh));
						if(heapPatterns.size() == topK) {
							mincoh = heapPatterns.peek().getSecond();
						}
					}
					else if(qcoh> mincoh) {
						heapPatterns.poll();
						heapPatterns.add(new Pair<SPattern,Double>(X,qcoh));
						mincoh =  heapPatterns.peek().getSecond();
					}
				}
			}
			else { //branch-and-bound
				if(!pruningOf && prune(X,P,Y,mincoh)) {
					continue;
				}
				stack.add(new Triple<>(X,P,new ArrayList<Integer>(Y.subList(1, Y.size()))));
				if(X.length() != maxsize) {
					int nextItem = Y.get(0);
					SPattern Z = new SPattern(X,nextItem); 
					List<Window> projectionZ = projectSparse(Z,P);
					List<Integer> itemsZ = projectCandidates(Z,projectionZ);
					stack.add(new Triple<>(Z,projectionZ,itemsZ));
				}
			}
		}
		timer.end();
		List<Pair<SPattern,Double>> sorted = new ArrayList<>();
		while(!heapPatterns.isEmpty()) {
			Pair<SPattern,Double> pattern = heapPatterns.poll();
			sorted.add(pattern);
		}
		//sort on support AND cohesion
		Collections.sort(sorted, new Comparator<Pair<SPattern,Double>>(){
			@Override
			public int compare(Pair<SPattern, Double> o1, Pair<SPattern, Double> o2) {
				return (int)(10000 * o1.getSecond() - 10000 * o2.getSecond() + support(o1.getFirst().pattern) - support(o2.getFirst().pattern));
			}});
		File output = new File("./temp/qcsp_patterns_" + FilenameUtils.getBaseName(this.inputSequence.getName()) + ".csv");
		BufferedWriter writer = new BufferedWriter(new FileWriter(output));
		writer.write("sequence;q-cohesion;support;length\n");
		for(int i=sorted.size()-1; i>=0; i--) {
			Pair<SPattern, Double> pattern =  sorted.get(i);
			writer.write(String.format("%s;%5.3f;%d;%d\n", toString(pattern.getFirst().pattern), pattern.getSecond(), support(pattern.getFirst().pattern), pattern.getFirst().length()));
		}
		writer.close();
		System.out.println("============================");
		System.out.println("QC Patterns:");
		IOUtils.printHead(output, 1000);
		System.out.println("end QC Patterns:");
		System.out.println("============================");
		return output;
	}

	/**
	 * Carefull for special cases: 
	 * 1) Overlapping windows: e.g. X=(a,b) and S=(a,a,b,b)" -> count both b's only once
	 * 2) A has multiple 'roles': X=(a,b,a) and S=(a,b,a,_,_,a,b,a) -> last a has minwin=3, not infinity 
	 * @param X
	 * @param projectionX
	 * @return
	 */
	public double quantileCohesionComputedOnProjection(SPattern X, List<Window> projectionX) {
		long maxwinSeconds = this.alphaSeconds * X.length();
		//even more simple? 
		List<List<Integer>> occurrences = computeAllOccurrences(X, projectionX, maxwinSeconds);
		int count = computeMinimalWindowsLengthsBasedOnProjectionSparse(occurrences);
		//Step 5: compute qcohesion
		int supportX = support(X.pattern);
		double qcoh = count / (double)supportX;
		return qcoh;
	}

	//compute minimum window at t for each occurrence
	//only called at dfs to compute cohesion if Y is empty
	//change Len 21-11-2017:  Performance tweak b), no need to compute map... only need counts here!
	private int computeMinimalWindowsLengthsBasedOnProjectionSparse(List<List<Integer>> occurrences) {
		Set<Integer> uniquePositions = new HashSet<Integer>();
		for(List<Integer> occurrence: occurrences)
		{
			for(Integer pos: occurrence) {
				uniquePositions.add(pos);
			}
		}
		return uniquePositions.size();
	}

	private int computeNumberOfMinimalWindowsBasedOnProjectionSparseSmaller(List<List<Integer>> occurrences,  SPattern X, Set<Integer> X_None_overlapping_items) {
		int countSmallWindowsNonOverlapping = 0;
		List<Integer> indexes = new ArrayList<Integer>();
		for(Integer item: X_None_overlapping_items) {
			indexes.add(X.pattern.indexOf(item));
		}
		for(List<Integer> occurrence: occurrences) {
			for(int i=0; i<occurrence.size(); i++) {
				Integer item = X.pattern.get(i);
				if(X_None_overlapping_items.contains(item)) {
					countSmallWindowsNonOverlapping++;
				}
			}
		}
		return countSmallWindowsNonOverlapping;
	}

	private List<List<Integer>> computeAllOccurrences(SPattern X, List<Window> projectionX, long maxwinSeconds) {
		//Step 1: shorten windows
		List<Window> shorterWindows = new ArrayList<Window>();
		for(Window window: projectionX) {
			long diffTime = diffTime(window.t, window.a-1);
			if(diffTime > maxwinSeconds)
				continue;
			//Math.min(window.b, window.t + maxwin);
			int end = getNextPositionLimitedByTime(window.t, maxwinSeconds);
			shorterWindows.add(new Window(window.t, end, end));
		}
		//Step 2: count all variations, level-wise -> do small DFS search
		//e.g. a0 -> b1, b2
		//			a0 -> b1 ->c1
		//			a0 -> b2 -> c1
		//     a1 -> b2, b2
		// 		     ...
		List<Pair<List<Integer>,Window>> stack = new ArrayList<>();
		for(Window window: shorterWindows) {
			List<Integer> X_poslist = makeList(window.t);
			stack.add(new Pair<>(X_poslist, new Window(window.t+1, window.b, window.b)));
		}
		List<List<Integer>> occurrences = new ArrayList<>();
		while(!stack.isEmpty()) {
			Pair<List<Integer>,Window> top = stack.remove(stack.size()-1);
			List<Integer> poslist = top.getFirst();
			Window window = top.getSecond();
			if(poslist.size() == X.length()) {
				occurrences.add(poslist);
			}
			else {
				int currentItem = X.pattern.get(poslist.size());
				for(int i=window.t; i<window.b; i++) {
					SequenceItem sItem = this.sequenceList.get(i);
					if(sItem.items.contains(currentItem)) {
						List<Integer> newPoslist = new ArrayList<Integer>(poslist);
						newPoslist.add(i);
						stack.add(new Pair<>(newPoslist,new Window(i+1,window.b,window.b)));
					}
				}
			}
		}
		return occurrences;
	}

	private List<Window> projectSparse(SPattern Z, List<Window> projectionX) {
		if(Z.length() == 1) {
			List<Integer> positions = this.projection1.get(Z.pattern.get(0));
			List<Window> windows = new ArrayList<Window>(positions.size());
			long maxwinInSeconds = this.alphaSeconds * this.maxsize;
			for(int pos: positions) {
				//windows.add(new Window(pos, pos+1, Math.min(pos+maxwin,this.sequenceList.size())));
				windows.add(new Window(pos, pos+1, getNextPositionLimitedByTime(pos,maxwinInSeconds)));
			}
			return windows;
		}
		else {
			List<Window> windows = new ArrayList<Window>(projectionX.size());
			int lastItem = Z.pattern.get(Z.length()-1);
			for(Window window: projectionX) {
				int found = -1;
				for(int i=window.a; i < window.b; i++) {
					SequenceItem item = this.sequenceList.get(i);
					if(item.items.contains(lastItem)) {
						found = i;
						break;
					}
				}
				if(found != -1) {
					windows.add(new Window(window.t, found+1, window.b));
				}
			}
			return windows;
		}
	}

	private boolean prune(SPattern X, List<Window> P, List<Integer> Y, double mincoh) {
		if(X.length() < 2) {
			return false;
		}
		//mingap pruning
		boolean overlap = false;
		for(Integer item: X.pattern) {
			if(Y.contains(item)) {
				overlap = true;
				break;
			}
		}
		if(!overlap) {
			long mingapSeconds = computeMinGapSeconds(X,P);
			int lengthZMax = computeLengthZMax(X,P);
			if(mingapSeconds + lengthZMax > alphaSeconds * (lengthZMax + X.length())) {
				return true;
			}
		}
		//c_maxquan pruning
		//step 1: compute supportZMax
		Set<Integer> union = new HashSet<Integer>();
		union.addAll(X.pattern);
		union.addAll(Y);
		int supportZMax = support(union);
		//step 2: compute |Zmax|
		CountMap<Integer> YPlus = computeYPlus(X,P);
		int lengthZMax = X.length() + YPlus.getMap().size();
		//step 3: compute count windows
		long maxwinSeconds = alphaSeconds * lengthZMax;
		//returns list of position, and minimal window length
		Set<Integer> XNoneOverlapping = new HashSet<Integer>();
		XNoneOverlapping.addAll(X.pattern);
		XNoneOverlapping.removeAll(Y);
		int countSmallWindowsNonOverlapping = 0;
		//change Len: Performance tweak, If NonOverlapping.size() == 0 -> No need to compute minimal windows
		if(XNoneOverlapping.size() > 0) {
			List<List<Integer>> occurrences = computeAllOccurrences(X, P, maxwinSeconds);
			countSmallWindowsNonOverlapping = computeNumberOfMinimalWindowsBasedOnProjectionSparseSmaller(occurrences, X, XNoneOverlapping);
		}
		int countLargeWindows = support(XNoneOverlapping) - countSmallWindowsNonOverlapping;
		double maxQuantileCohesion = 1.0 - (countLargeWindows / (double)supportZMax);
		if(maxQuantileCohesion <= mincoh) {
			return true;
		}
		return false;
	}

	private List<Integer> projectCandidates(SPattern z, List<Window> projectionZ) {
		//union of all items in suffixes
		CountMap<Integer> supportInP = new CountMap<Integer>();
		for(Window window: projectionZ) {
			for(int i=window.a; i < window.b; i++) {
				SequenceItem sItem = this.sequenceList.get(i);
				for(Integer item: sItem.items) {
					supportInP.add(item);
				}
			}
		}
		//sort on descending support in P
		return getItemsSorted(supportInP, false);
	}


	private int getNextPositionLimitedByTime(int pos, long maxwinInSeconds){
		int endPos = pos+1;
		for(; endPos<this.sequenceList.size(); endPos++) {
			if(diffTime(pos,endPos) > maxwinInSeconds) {
				break;
			}
		}
		return endPos;
	}

	private long diffTime(int posA, int posB) {
		//assuming posA < posB
		Date timeA = this.sequenceList.get(posA).getTimestamp();
		Date timeB = this.sequenceList.get(posB).getTimestamp();
		long secondsDiff = (timeB.getTime()-timeA.getTime())/1000; 
		return secondsDiff;
	}

	private CountMap<Integer> computeYPlus(SPattern X, List<Window> projectionX){
		CountMap<Integer> multiSetJoin = new CountMap<Integer>();
		for(Window window: projectionX) {
			CountMap<Integer> multiset = new CountMap<>();
			for(int i=window.a; i < window.b; i++) {
				SequenceItem sItem = this.sequenceList.get(i);
				for(Integer item: sItem.items) {
					multiset.add(item);
				}
			}
			for(Entry<Integer,Integer> entry: multiset.getMap().entrySet()) {
				Integer previousCount = multiSetJoin.get(entry.getKey());
				Integer newCount = max(entry.getValue(), previousCount);
				multiSetJoin.getMap().put(entry.getKey(),newCount);
			}
		}
		return multiSetJoin;
	}

	private Integer max(Integer a, Integer bOrNull) {
		if(bOrNull == null)
			return a;
		else
			return Math.max(a, bOrNull);
	}

	private long computeMinGapSeconds(SPattern X, List<Window> P) {
		long mingap = Integer.MAX_VALUE;
		for(Window window: P) {
			//mingap = Math.min(window.a - window.t,mingap);
			mingap = Math.min(diffTime(window.t, window.a-1), mingap);
		}
		return mingap;
	}

	private int computeLengthZMax(SPattern X, List<Window> P){
		int maxlen = 0;
		for(Window window: P) {
			int start = window.a;
			int end = window.b;
			maxlen = Math.max(maxlen, end-start);
		}
		return X.length() + maxlen;
	}

	/**
	 * @param sequence
	 * @param labels
	 * @throws IOException
	 */
	private void loadData(File sequence, File labels) throws IOException {
		//Step 1: Load labels
		FileStream fs2 = new FileStream(labels,' ','\n');
		this.labelsList = new ArrayList<String>();
		String label = fs2.nextToken();
		while(label != null){
			labelsList.add(label);
			label = fs2.nextToken();
		}
		System.out.println("Loaded labels");
		//Step 2: Load sequence
		this.sequenceList = new ArrayList<SequenceItem>((int)Utils.countLines(sequence));
		FileStream fs = new FileStream(sequence,'\n');
		String token = fs.nextToken();
		while(token !=null){
			SequenceItem item = new SequenceItem(token);
			sequenceList.add(item);
			token = fs.nextToken();
		}
		System.out.println("Loaded sequence");
		//Step 3: Pre-compute support
		this.support = new CountMap<>();
		for(SequenceItem sequenceItem: sequenceList) {
			for(Integer item: sequenceItem.getItems())
				support.add(item);
		}
		//Step 4: sort on ascending support
		this.itemsSortedOnAscendingSupport= getItemsSorted(support,true);
		//Step 5: Pre-compute poslist for level 1 projection
		this.projection1 = new ListMap<>();
		for(int idx=0; idx < sequenceList.size(); idx++) {
			//Integer item = sequenceList.get(idx);
			SequenceItem sequenceItem = sequenceList.get(idx);
			for(Integer item: sequenceItem.items)
				projection1.put(item, idx);
		}
		System.out.println("Loaded data");
	}

	//TODO: Bugfix for sparse (distinct on time t, if two items occur at same time)
	private int support(Collection<Integer> items) {
		int support = 0;
		for(int item: items) {
			support += this.support.get(item);
		}
		return support;
	}

	private List<Integer> getItemsSorted(CountMap<Integer> support, boolean ascending){
		List<Entry<Integer,Integer>> lst = new ArrayList<>(support.getMap().entrySet());
		final int sign = ascending? 1:-1;
		Collections.sort(lst, new Comparator<Entry<Integer,Integer>>() {

			@Override
			public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2) {
				return sign * (o1.getValue() - o2.getValue());
			}
		});
		List<Integer> keys = new ArrayList<Integer>();
		for(Entry<Integer,Integer> entry: lst)
			keys.add(entry.getKey());
		return keys;
	}


	private <T> ArrayList<T> makeList(T first){
		ArrayList<T> lst = new ArrayList<T>();
		lst.add(first);
		return lst;
	}

	private String toString(List<Integer> X) {
		StringBuffer buff = new StringBuffer();
		buff.append("(");
		for(int i=0; i<X.size()-1; i++) {
			buff.append(this.labelsList.get(X.get(i)));
			buff.append(",");
		}
		if(X.size() > 0) {
			Integer lastItem = X.get(X.size()-1);
			buff.append(this.labelsList.get(lastItem));
		}
		buff.append(")");
		return buff.toString();
	}

	private static Comparator<Pair<SPattern,Double>> heapComparator = new Comparator<Pair<SPattern,Double>>() {

		@Override
		public int compare(Pair<SPattern, Double> o1, Pair<SPattern, Double> o2) {
			return Double.compare(o1.getSecond(),o2.getSecond()); //sort on descending qcohesion
		}
	};

}