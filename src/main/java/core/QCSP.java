package core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;

import util.CountMap;
import util.DatLabFileToNormalFile;
import util.FileStream;
import util.ListMap;
import util.Pair;
import util.Timer;
import util.Triple;
import util.Utils;

/**
 * Main implementation QCSP that takes consecutive event series (with possible gaps) as input.
 * Note: gaps should be encoded as 'None' or 'null'
 *  
 * For sparse timestamped input use QCSP_Sparse.
 * 
 * @author lfereman
 */
public class QCSP {

	public static void main(String[] args) throws Exception {
		//command = ["java", "-cp", ":".join(classpath), "core.QCSP",  inputFile, str(alpha),  str(maxsize), str(topK), _bool2str(pruningOf), "-Xmx4G"]	
		if(args.length <= 4) {
			System.err.println("QCSP expects:>> QCSP filename alpha maxsize topk pruningOf.\nalpha: double between 1.0 and ...\nmaxsize: integer\ntop: integer\npruningOf: true/false (default true)\ne.g. QCSP ./data/moby_fragment.txt 2.0 5 100 false");
			System.out.println(Arrays.asList(args));
			return;
		}
		QCSP algo = new QCSP(new File(args[0]), Double.valueOf(args[1]), Integer.valueOf(args[2]), Integer.valueOf(args[3]));
		if(args.length >= 5) {
			boolean pruningOf = Boolean.valueOf(args[4]);
			algo.pruningOf = pruningOf;
		}
		algo.run(true);
	}

	//data
	private List<Integer> sequenceList; //sequence of items
	private List<String> labelsList; //sequence of strings, e.g. item with id '0' is mapped to labelsList[0]
	CountMap<Integer> support; //maps item id to support in S (e.g. support.get(0) = 200)
	private List<Integer> itemsSortedOnAscendingSupport; //items sorted (asscending support), e.g. erps-kwerps,....,moby
	private ListMap<Integer,Integer> projection1; //projection on first level, e.g. projection1.get(0) returns list of positions, that is [0, 102, 1003, 2003, 2010, 191919] etc

	//parameters
	double alpha;
	int maxsize; 
	int topK;
	
	//additional parameter for experimental purpose: turns pruning of
	boolean pruningOf = false;
	
	/**
	 * Main paramters, passed to constructor
	 * 
	 * @param sequence
	 * @param labels
	 * @param alpha
	 * @param maxsize
	 * @param topK
	 * @throws IOException
	 */
	public QCSP(File singleFile, double alpha, int maxsize, int topK) throws IOException {
		//parameters
		this.alpha = alpha;
		this.maxsize = maxsize;
		this.topK = topK;
		//convert data
		File seq = new File("./temp", singleFile.getName() + "_gap.seq");
		File label = new File("./temp", singleFile.getName() + "_gap.lab");
		DatLabFileToNormalFile.convertToDataLab(singleFile, seq, label);
		//read data
		loadData(seq,label);
		return;
	}

	private class Window{
		public int t;
		public int a; //note: a is start of SUFFIX
		public int b; //note: b is end of window and SUFFIX
		public Window(int t, int a, int b) {
			this.t= t; this.a=a; this.b=b;
		}
	}

	private final static class SPattern{
		public List<Integer> pattern;
		public SPattern() {
			this.pattern = new ArrayList<Integer>();
		}
		public SPattern(SPattern prefix, Integer item){
			this.pattern = new ArrayList<Integer>(prefix.length() + 1);
			for(int i=0; i<prefix.length();i++) {
				this.pattern.add(prefix.pattern.get(i));
			}
			this.pattern.add(item);
		}
		final public int length() {
			return pattern.size();
		}
	}

	private List<Window> init = makeList(new Window(-1,-1,-1));

	/**
	 * @param debug if true shows progress
	 */
	public void run(boolean debug) {
		Timer timer = new Timer("QSCP.run()");
		System.out.format("Parameters: alpha=%f, maxsize=%d, top-k=%d, pruningOf=%s\n", this.alpha, this.maxsize, this.topK, this.pruningOf);
		PriorityQueue<Pair<SPattern,Double>> heapPatterns = new PriorityQueue<>(topK, heapComparator);
		double mincoh = 0.0;
		ArrayList<Triple<SPattern, List<Window>, List<Integer>>> stack = new ArrayList<>();
		stack.add(new Triple<>(new SPattern(),init,itemsSortedOnAscendingSupport));
		long iterations = 0;
		long leafs = 0;
		while(!stack.isEmpty()) {
			iterations++;
			Triple<SPattern, List<Window>, List<Integer>> top = stack.remove(stack.size()-1);
			SPattern X = top.getFirst();
			List<Window> P = top.getSecond();
			List<Integer> Y = top.getThirth();
			if(debug && iterations % 10000000 == 0)
			{
				int currentIndex = itemsSortedOnAscendingSupport.indexOf(X.pattern.get(0));
				timer.progress(currentIndex, itemsSortedOnAscendingSupport.size());
				System.out.format("Iterations:%-10d, #Patterns: %d, worst: %s, min_coh:%.3f, \n",  iterations/10000000, heapPatterns.size(), toString(heapPatterns.peek().getFirst().pattern), mincoh);
			}
			if(Y.size() == 0) { //leaf
				if(X.length() > 1) {
					leafs++;
					double qcoh = quantileCohesionComputedOnProjection(X,P);
					if(heapPatterns.size() < topK) {
						heapPatterns.add(new Pair<SPattern,Double>(X,qcoh));
						if(heapPatterns.size() == topK) {
							mincoh = heapPatterns.peek().getSecond();
						}
					}
					else{
						if(qcoh> mincoh) {
							heapPatterns.poll();
							heapPatterns.add(new Pair<SPattern,Double>(X,qcoh));
							mincoh = heapPatterns.peek().getSecond();
						}
					}
				}
			}
			else { //branch-and-bound
				if(!pruningOf && prune(X,P,Y,mincoh)) {
					continue;
				}
				stack.add(new Triple<>(X,P,new ArrayList<Integer>(Y.subList(1, Y.size()))));
				if(X.length() != maxsize) {
					int nextItem = Y.get(0);
					SPattern Z = new SPattern(X,nextItem); 
					List<Window> projectionZ = project(Z,P);
					List<Integer> itemsZ = projectCandidates(Z,projectionZ);
					stack.add(new Triple<>(Z,projectionZ,itemsZ));
				}
			}
		}
		System.out.println("Iterations: " + iterations);
		System.out.format("Leaves pruningOf=%s maxsize=%s: %s\n",pruningOf, maxsize, leafs);
		timer.end();
		System.out.println("============================");
		System.out.println("QC Patterns:");
		List<Pair<SPattern,Double>> sorted = new ArrayList<>();
		while(!heapPatterns.isEmpty()) {
			Pair<SPattern,Double> pattern = heapPatterns.poll();
			sorted.add(pattern);
		}
		//sort on support AND cohesion
		Collections.sort(sorted, new Comparator<Pair<SPattern,Double>>(){

			@Override
			public int compare(Pair<SPattern, Double> o1, Pair<SPattern, Double> o2) {
				return (int)(10000 * o1.getSecond() - 10000 * o2.getSecond() + support(o1.getFirst().pattern) - support(o2.getFirst().pattern));
			}});

		for(int i=sorted.size()-1; i>=0; i--) {
			Pair<SPattern, Double> pattern =  sorted.get(i);
			System.out.format("%-32s: %-5.3f %-5d \n", toString(pattern.getFirst().pattern), pattern.getSecond(), support(pattern.getFirst().pattern));
		}
		System.out.println("end QC Patterns:");
		System.out.println("============================");
	}
	
	/**
	 * Carefull for special cases: 
	 * 1) Overlapping windows: e.g. X=(a,b) and S=(a,a,b,b)" -> count both b's only once
	 * 2) A has multiple 'roles': X=(a,b,a) and S=(a,b,a,_,_,a,b,a) -> last a has minwin=3, not infinity 
	 * @param X
	 * @param projectionX
	 * @return
	 */
	public double quantileCohesionComputedOnProjection(SPattern X, List<Window> projectionX) {
		int maxwin = (int)Math.floor(this.alpha * X.length());
		Map<Integer, Integer> minWinAtT = computeMinimalWindowsBasedOnProjection1(X, projectionX, maxwin);
		//Step 4: count number of minwindows
		int count = minWinAtT.keySet().size(); 
		//Step 5: compute qcohesion
		int supportX = support(X.pattern);
		double qcoh = count / (double)supportX;
		return qcoh;
	}

	private Map<Integer, Integer> computeMinimalWindowsBasedOnProjection1(SPattern X, List<Window> projectionX, int maxwin) {
		//TODO: Copy/paste with next
		//Step 1: shorten windows
		List<Window> shorterWindows = new ArrayList<Window>();
		for(Window window: projectionX) {
			if(window.a - window.t > maxwin)
				continue;
			int end = Math.min(window.b, window.t + maxwin);
			shorterWindows.add(new Window(window.t, end, end));
		}
		//Step 2: count all variations, level-wise -> do small DFS search
		//e.g. a0 -> b1, b2
		//			a0 -> b1 ->c1
		//			a0 -> b2 -> c1
		//     a1 -> b2, b2
		// 		     ...
		List<Pair<List<Integer>,Window>> stack = new ArrayList<>();
		for(Window window: shorterWindows) {
			List<Integer> X_poslist = makeList(window.t);
			stack.add(new Pair<>(X_poslist, new Window(window.t+1, window.b, window.b)));
		}
		List<List<Integer>> occurrences = new ArrayList<>();
		while(!stack.isEmpty()) {
			Pair<List<Integer>,Window> top = stack.remove(stack.size()-1);
			List<Integer> poslist = top.getFirst();
			Window window = top.getSecond();
			if(poslist.size() == X.length()) {
				occurrences.add(poslist);
			}
			else {
				int currentItem = X.pattern.get(poslist.size());
				for(int i=window.t; i<window.b; i++) {
					Integer item = this.sequenceList.get(i);
					if(item == null)
						continue;
					if(item  == currentItem) {
						List<Integer> newPoslist = new ArrayList<Integer>(poslist);
						newPoslist.add(i);
						stack.add(new Pair<>(newPoslist,new Window(i+1,window.b,window.b)));
					}
				}
			}
		}
		//Step 3: compute minimum window at t for each occurrence
		Map<Integer,Integer> minWinAtT = new HashMap<Integer,Integer>();
		for(List<Integer> occurrence: occurrences) {
			Integer mwin = occurrence.get(occurrence.size()-1) - occurrence.get(0);
			for(Integer pos: occurrence) {
				minWinAtT.put(pos, minWindow(mwin, minWinAtT.get(pos)));
			}
		}
		return minWinAtT;
	}

	//Compute minimal windows. Less detailed in paper.
	List<Window> shorterWindowsCache = new ArrayList<>();
	List<Pair<List<Integer>,Window>> stack = new ArrayList<>();
	List<List<Integer>> occurrences = new ArrayList<>();
	Map<Integer,Integer> itemAtT = new HashMap<Integer,Integer>();
	private int computeNumberOfMinimalWindowsBasedOnProjection(SPattern X, Set<Integer> XNoneOverlapping, List<Window> projectionX, int lengthZMax) {
		int maxwin = (int)Math.floor(alpha * lengthZMax);
		//Step 1: shorten windows
		List<Window> shorterWindows = projectionX;
		if(lengthZMax < maxsize){ 
			shorterWindows = shorterWindowsCache;
			shorterWindowsCache.clear();
			for(Window window: projectionX) {
				if(window.a - window.t > maxwin)
					continue;
				int end = Math.min(window.b, window.t + maxwin);
				shorterWindows.add(new Window(window.t, end, end));
			}
		}
		//Step 2: count all variations, level-wise -> do small DFS search
		stack.clear();
		for(Window window: shorterWindows) {
			List<Integer> X_poslist = new ArrayList<Integer>(X.length());
			X_poslist.add(window.t);
			stack.add(new Pair<>(X_poslist, new Window(window.t+1, window.b, window.b)));
		}
		occurrences.clear();
		while(!stack.isEmpty()) {
			Pair<List<Integer>,Window> top = stack.remove(stack.size()-1);
			List<Integer> poslist = top.getFirst();
			Window window = top.getSecond();
			if(poslist.size() == X.length()) {
				occurrences.add(poslist);
			}
			else {
				int currentItem = X.pattern.get(poslist.size());
				for(int i=window.t; i<window.b; i++) {
					Integer item = this.sequenceList.get(i);
					if(item == null)
						continue;
					if(item  == currentItem) {
						List<Integer> newPoslist = new ArrayList<Integer>(poslist);
						newPoslist.add(i);
						stack.add(new Pair<>(newPoslist,new Window(i+1,window.b,window.b)));
					}
				}
			}
		}
		//compute for each item, number of minimal windows
		itemAtT.clear();
		for(List<Integer> occurrence: occurrences) {
			for(int i=0; i<occurrence.size(); i++) {
				itemAtT.put(occurrence.get(i), X.pattern.get(i));
			}
		}
		int countSmallWindowsNonOverlapping = 0;
		for(Entry<Integer,Integer> entry: itemAtT.entrySet()) {
			Integer item = entry.getValue();
			if(XNoneOverlapping.contains(item)) 
				countSmallWindowsNonOverlapping++;
		}
		return countSmallWindowsNonOverlapping;
	}
	
	//Len: optimalisation ... was originally in python... less pruning, but faster
	/*
	private int computeNumberOfMinimalWindowsBasedOnProjectionApprox(SPattern X, Set<Integer> XNoneOverlapping, List<Window> projectionX, int lengthZMax) {
		//Step 2: simple count
		Set<Integer> positions = new HashSet<Integer>();
		for(Window window: projectionX) {
			for(int i=window.t; i<window.b; i++) {
				Integer item = this.sequenceList.get(i);
				if(XNoneOverlapping.contains(item)) {
					positions.add(i);
				}
			}
		}
		int countSmallWindowsNonOverlapping = positions.size();
		return countSmallWindowsNonOverlapping;
	}
	*/

	private int minWindow(Integer a, Integer bOrNull) {
		if(bOrNull == null) 
			return a;
		else 
			return Math.min(a, bOrNull);
	}

	private List<Window> project(SPattern Z, List<Window> projectionX) {
		if(Z.length() == 1) {
			List<Integer> positions = this.projection1.get(Z.pattern.get(0));
			List<Window> windows = new ArrayList<Window>(positions.size());
			int maxwin = (int)Math.floor(this.alpha * this.maxsize);
			for(int pos: positions) {
				windows.add(new Window(pos, pos+1, Math.min(pos+maxwin,this.sequenceList.size())));
			}
			return windows;
		}
		else {
			List<Window> windows = new ArrayList<Window>(projectionX.size());
			int lastItem = Z.pattern.get(Z.length()-1);
			for(Window window: projectionX) {
				int found = -1;
				for(int i=window.a; i < window.b; i++) {
					Integer item = this.sequenceList.get(i);
					if(item == null)
						continue;
					if(item == lastItem) {
						found = i;
						break;
					}
				}
				if(found != -1) {
					windows.add(new Window(window.t, found+1, window.b));
				}
			}
			return windows;
		}
	}

	//compute Y
	private List<Integer> projectCandidates(SPattern z, List<Window> projectionZ) {
		//union of all items in suffixes
		CountMap<Integer> supportInP = new CountMap<Integer>();
		for(Window window: projectionZ) {
			for(int i=window.a; i < window.b; i++) {
				Integer item = this.sequenceList.get(i);
				if(item != null)
					supportInP.add(item);
			}
		}
		//sort on descending support in P
		return getItemsSorted(supportInP, false);
	}

	private boolean prune(SPattern X, List<Window> P, List<Integer> Y, double mincoh) {
		final int xLen = X.length();
		if(xLen < 2) {
			return false;
		}
		//mingap pruning
		boolean overlap = false;
		for(Integer item: X.pattern) {
			if(Y.contains(item)) {
				overlap = true;
				break;
			}
		}
		if(!overlap) {
			int lengthZMax = Math.min(computeLengthZMax(X,P),maxsize); //change len 2018-1-17
			int mingap = computeMinGap(X,P);
			if(mingap + lengthZMax> alpha * (lengthZMax + xLen)) {
				return true;
			}
		}
		//c_maxquan pruning
		//step 1: compute supportZMax
		Set<Integer> XNoneOverlapping = new HashSet<Integer>();
		XNoneOverlapping.addAll(X.pattern);
		XNoneOverlapping.removeAll(Y);
		if(XNoneOverlapping.size() == 0) {
			return false;
		}
		int supportXNoneOverlapping  = support(XNoneOverlapping);
		int supportZMax = supportXNoneOverlapping + support(Y);
		//step 2: compute |Zmax| 
		int maxsizeYPlus = computeYPlus(X,P,Y);
		int lengthZMaxCorrect = Math.min(xLen + maxsizeYPlus, maxsize); //MIN!
		//step 3: compute count windows
		int countSmallWindowsNonOverlapping = computeNumberOfMinimalWindowsBasedOnProjection(X, XNoneOverlapping, P, lengthZMaxCorrect);
		int countLargeWindows = supportXNoneOverlapping - countSmallWindowsNonOverlapping;
		double maxQuantileCohesion = 1.0 - (countLargeWindows / (double)supportZMax);
		if(maxQuantileCohesion <= mincoh) {
			return true;
		}
		return false;
	}

	private int computeYPlus(SPattern X, List<Window> projectionX, List<Integer> Y){
		int[] multisetCount = new int[Y.size()];
		int[] windowCounts = new int[Y.size()];
		for(Window window: projectionX) {
			for(int i=window.a; i < window.b; i++) {
				Integer item = this.sequenceList.get(i);
				if(item != null) {
					int idx = Y.indexOf(item);
					if(idx != -1) //could be the case, e.g. item is filtered out of Y in previous DFS loop, but still in projection
						windowCounts[idx]+=1;
				}
			}
			for(int i=0; i<Y.size(); i++) {
				multisetCount[i] = Math.max(windowCounts[i],  multisetCount[i]);
			}
		}
		//Important: NOT return multiSetJoin.getMap().size();
		int maxlen = 0;
		for(int count: multisetCount) {
			maxlen += count;
		}
		return maxlen;
	}

	private int computeMinGap(SPattern X, List<Window> P) {
		int mingap = Integer.MAX_VALUE;
		for(Window window: P) {
			mingap = Math.min(window.a - window.t,mingap);
		}
		return mingap;
	}

	private int computeLengthZMax(SPattern X, List<Window> P){
		int maxlen = 0;
		for(Window window: P) {
			//trim null elements
			int start = window.a;
			for(int i=window.a; i<window.b; i++) {
				if(this.sequenceList.get(i) == null){
					start +=1;
				}
				else {
					break;
				}
			}
			int end = window.b;
			for(int i=window.b-1; i>=window.a; i--) {
				if(this.sequenceList.get(i) == null){
					end -=1;
				}
				else {
					break;
				}
			}
			maxlen = Math.max(maxlen, end-start);
		}
		return X.length() + maxlen;
	}

	/**
	 * @param sequence
	 * @param labels
	 * @throws IOException
	 */
	private void loadData(File sequence, File labels) throws IOException {
		//Step 1: Load labels
		FileStream fs2 = new FileStream(labels,' ','\n');
		this.labelsList = new ArrayList<String>();
		String label = fs2.nextToken();
		while(label != null){
			labelsList.add(label);
			label = fs2.nextToken();
		}
		int idxNone = labelsList.indexOf("None");
		if(idxNone == -1) {
			idxNone = labelsList.indexOf("null");
		}
		//Step 2: Load sequence
		this.sequenceList = new ArrayList<Integer>((int)Utils.countLines(sequence));
		FileStream fs = new FileStream(sequence,' ','\n');
		String token = fs.nextToken();
		while(token !=null){
			Integer item = Integer.parseInt(token);
			sequenceList.add(item != idxNone?item: null);
			token = fs.nextToken();
		}

		//Step 3: Pre-compute support
		this.support = new CountMap<Integer>(); 
		for(Integer item: sequenceList) {
			if(item != null) {
				support.add(item);
			}
		}
		//Step 4: sort on ascending support
		this.itemsSortedOnAscendingSupport= getItemsSorted(support,true);
		//Step 5: Pre-compute poslist for level 1 projection
		this.projection1 = new ListMap<>();
		for(int idx=0; idx < sequenceList.size(); idx++) {
			Integer item = sequenceList.get(idx);
			if(item != null)
				projection1.put(item, idx);
		}
	}

	//TODO: Bugfix for sparse (distinct on time t, if two items occur at same time)
	private int support(Collection<Integer> items) {
		int support = 0;
		for(int item: items) {
			support += this.support.get(item);
		}
		return support;
	}

	private List<Integer> getItemsSorted(CountMap<Integer> support, boolean ascending){
		List<Entry<Integer,Integer>> lst = new ArrayList<>(support.getMap().entrySet());
		final int sign = ascending? 1:-1;
		Collections.sort(lst, new Comparator<Entry<Integer,Integer>>() {

			@Override
			public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2) {
				return sign * (o1.getValue() - o2.getValue());
			}
		});
		List<Integer> keys = new ArrayList<Integer>();
		for(Entry<Integer,Integer> entry: lst)
			keys.add(entry.getKey());
		return keys;
	}


	private <T> ArrayList<T> makeList(T first){
		ArrayList<T> lst = new ArrayList<T>();
		lst.add(first);
		return lst;
	}

	private String toString(List<Integer> X) {
		StringBuffer buff = new StringBuffer();
		buff.append("(");
		for(int i=0; i<X.size()-1; i++) {
			buff.append(this.labelsList.get(X.get(i)));
			buff.append(",");
		}
		if(X.size() > 0)
			buff.append(this.labelsList.get(X.get(X.size()-1)));
		buff.append(")");
		return buff.toString();
	}

	private static Comparator<Pair<SPattern,Double>> heapComparator = new Comparator<Pair<SPattern,Double>>() {

		@Override
		public int compare(Pair<SPattern, Double> o1, Pair<SPattern, Double> o2) {
			return Double.compare(o1.getSecond(),o2.getSecond()); //sort on descending qcohesion
		}
	};

}