package core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import util.DoubleMap;
import util.Utils;

public class PreProcessing {
	
	/**
	 * make differences wquences
	 */
	
	//similar to DatLabFileToNormalFile
	public static void convertToDataLabMultipleSequences(File input, File outputDataSequenceFile, File outputDataDictionaryFile) throws IOException{
		DoubleMap<String,Integer> dict = new DoubleMap<String,Integer>(); //abstraction over TreeMap
		List<String> lines = Utils.readFileUntil(input,Integer.MAX_VALUE);
		int index = 0;
		for(String line: lines) {
			for(String token: line.split("\\s+")) {
				if(dict.get(token)==null){
					dict.put(token, index++);
				}
			}
		}
		List<Integer> sequence = new ArrayList<Integer>();
		for(String line: lines) {
			for(String token: line.split("\\s+")) {
				int idx = dict.get(token);
				sequence.add(idx);
			}
			//NEW! Compare to Da
			sequence.add(-1);
		}
		//save files
		//outputDataDictionaryFile, ends with '.lab'
		//format token1\ntoken2...
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputDataDictionaryFile));
		for(int i=0; i<dict.keySet().size(); i++){
			writer.write(dict.getByValue(i));
			writer.write("\n");
		}
		writer.close();
		System.out.println("Saved " + outputDataDictionaryFile);
		writer = new BufferedWriter(new FileWriter(outputDataSequenceFile));
		for(Integer i: sequence){
			writer.write(String.valueOf(i));
			writer.write(" ");
		}
		writer.close();
		System.out.println("Saved " + outputDataSequenceFile);
	}
	
	//for spmf
	public static void convertToDataLabMultipleSequencesSPMF(File input, File outputDataSequenceFile, File outputDataDictionaryFile) throws IOException{
		DoubleMap<String,Integer> dict = new DoubleMap<String,Integer>(); //abstraction over TreeMap
		List<String> lines = Utils.readFileUntil(input,Integer.MAX_VALUE);
		int index = 1; //for GoKRIMP start with 1
		for(String line: lines) {
			if(line.trim().equals(""))
				continue;
			for(String token: line.split("\\s+")) {
				if(token.trim().equals(""))
					continue;
				if(dict.get(token)==null){
					dict.put(token, index++);
				}
			}
		}
		List<Integer> sequence = new ArrayList<Integer>();
		for(String line: lines) {
			if(line.trim().equals(""))
				continue;
			for(String token: line.split("\\s+")) {
				if(token.trim().equals(""))
					continue;
				int idx = dict.get(token);
				sequence.add(idx);
				sequence.add(-1);
			}
			sequence.add(-2);
		}
		//save files
		//outputDataDictionaryFile, ends with '.lab'
		//format token -1 ntoken2 -1 ... -2
		if(!outputDataDictionaryFile.getParentFile().exists()) {
			outputDataDictionaryFile.getParentFile().mkdirs();
		}
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputDataDictionaryFile));
		for(int i=1; i<=dict.keySet().size(); i++){ //start with 1
			writer.write(dict.getByValue(i));
			writer.write("\n");
		}
		writer.close();
		System.out.println("Saved " + outputDataDictionaryFile);
		if(!outputDataSequenceFile.getParentFile().exists())
			outputDataSequenceFile.getParentFile().mkdirs();
		writer = new BufferedWriter(new FileWriter(outputDataSequenceFile));
		for(Integer i: sequence){
			writer.write(String.valueOf(i));
			writer.write(" ");
			if(i == -2) {
				writer.write("\n");
			}
		}
		writer.close();
		System.out.println("Saved " + outputDataSequenceFile);
	}
}
