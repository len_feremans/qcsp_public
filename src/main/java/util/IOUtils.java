package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Generic utils for:
 *   Saving/loading text files.
 *   Finding string in files
 *   Filename manipulation
 *   
 * @author lfereman
 *
 */ 
public class IOUtils {
	
	public enum Encoding{
		DEFAULT,
		UTF_8,
		ISO_8859_1
	}
	
	public static void saveFile(String s, File file) throws IOException
	{
		saveFile(s, file, Encoding.DEFAULT);
	}
	
	public static void saveFile(String s, File file, Encoding encoding) throws IOException
	{
		if(!file.getParentFile().exists())
			file.getParentFile().mkdirs();
		BufferedWriter writer = null;
		if(encoding.equals(Encoding.DEFAULT))
			 writer = new BufferedWriter(new FileWriter(file));
		else if(encoding.equals(Encoding.UTF_8))
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		else
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "ISO-8859-1"));
		writer.write(s);
		writer.flush();
		writer.close();
		System.out.format("Saved %s\n", file.getName());
	}

	public static void printHead(File file, int n) throws IOException{	
		System.out.println("====== HEAD " + file.getName() + " ========");
		List<String> lines = readFileUntil(file, n);
		for(String line: lines)
			System.out.println(line);
	}
	
	public static List<String> readFileUntil(File file, int lineNumber) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		List<String> lines = new ArrayList<String>();
		String current = reader.readLine();
		int line = 1;
		while(current != null)
		{
			lines.add(current);
			current = reader.readLine(); 
			line++;
			if(line >= lineNumber)
				break;
		}
		reader.close();
		return lines;
	}
	
}
