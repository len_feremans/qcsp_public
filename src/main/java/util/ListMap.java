package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ListMap<K,V> {

	private Map<K,List<V>> map = new HashMap<K,List<V>>();
	
	public void put(K key, V value){
		List<V> lst = map.get(key);
		if(lst == null){
			lst = new ArrayList<V>();
			map.put(key, lst);
		}
		lst.add(value);
	}
	
	public void putAll(K key, List<V> values){
		List<V> lst = map.get(key);
		if(lst == null){
			lst = values;
			map.put(key, lst);
		}
		lst.addAll(values);
	}
	
	public void putList(K key, List<V> lst){
		map.put(key, lst);
	}
	
	
	public void remove(K key){
		map.remove(key);
	}
	
	public List<V> get(K key){
		return map.get(key);
	}
	
	public Set<K> keySet(){
		return map.keySet();
	}

	public Set<Entry<K,List<V>>> entrySet() {
		return map.entrySet();
	}
	
	public String toString(){
		return map.toString();
	}
}
