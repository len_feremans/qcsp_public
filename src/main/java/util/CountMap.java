package util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CountMap<K> {

	private Map<K,Integer> map = new HashMap<K,Integer>();
	
	public void add(K key){
		map.put(key, get(key) + 1);
	}
	
	public void remove(K key){
		map.remove(key);
	}
	
	public int get(K key){
		Integer i = map.get(key);
		return (i == null)?0:i;
	}
	
	public Map<K,Integer> getMap(){
		return map;
	}
	
	public Set<K> keySet(){
		return map.keySet();
	}
		
	public void clear() {
		map.clear();
	}
}
