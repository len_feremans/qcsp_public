package util;


public class Timer {

	public static boolean VERBOSE = true;
	private long start;
	private long intermediateStart;
	private String process;
	
	public Timer(String process){
		this.start = System.currentTimeMillis();
		this.intermediateStart = this.start;
		this.process = process;
		if(process.length() > 20){
			this.process = process.substring(0, 20) + "...";
		}
		if(VERBOSE)
			System.out.format(">Started %s\n", process);
	}
	
	public void progress(int i){
		progress(null, i, -1);
	}
	
	public void progress(String message, int i){
		progress(message, i, -1);
	}
	
	public void progress(long i, long total){
		progress(null, i, total);
	}
	
	public void progress(String message, long i, long total){
		long end = System.currentTimeMillis();
		long elapsed = (end - intermediateStart);
		long elapsedTotal = (end - start);
		if(VERBOSE){
			String estimate = "";
			if(total<i)
				total = i;
			if(total > 10){
				//rule of three:
				//   if processing k items takes s milis
				//   then processing 1 item takes s/k milis
				//   and estimated time is (total - k) X s/k milis
				long estimatedMilis =  Math.round((total - i) * (elapsedTotal/(double)i)); 
				estimate = " Expected " +  Utils.milisToStringReadable(estimatedMilis);
			}
			System.out.format(" Process %s %s: %.2f %% items. Elapsed %s. Total %s.%s\n", 
				process, message == null? "": message, i/(double)total * 100,
				Utils.milisToStringReadable(elapsed),
				Utils.milisToStringReadable(elapsedTotal),
				estimate);
		}
		this.intermediateStart = System.currentTimeMillis(); 
	}
	
	
	public long end(){
		long end = System.currentTimeMillis();
		long elapsed = (end - start);
		if(VERBOSE)
			System.out.format("<Finished %s. Took %s\n", process, 
					Utils.milisToStringReadable(elapsed));
		return elapsed;
				
	}
	



}
